import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator,StackNavigator,createStackNavigator} from 'react-navigation';
import Home from './Tab/Home';
import Listings from './Tab/Listings';
import Events from './Tab/Events';
import Accounts from './Tab/Accounts';
import More from './Tab/More';
import Splash from './Splash';
import Icon from 'react-native-vector-icons/FontAwesome';
export default class App extends Component {
  render() {
    return (
      <AppStackNavigator />
    );
  }
}
const AppStackNavigator = createStackNavigator({
  InternalFlow: { 
      screen: StackNavigator({
          Splash:Splash
        }
      
      )
},
BasicTab: { 
  screen: createBottomTabNavigator(
    {
      Home:{screen:Home,
  navigationOptions:{
    tabBarLabel:'Home',
    tabBarIcon: ({tintColor})=>(
      <Icon name="home" size={24} />
    )
  }
  },

  Listings:{screen:Listings,
    navigationOptions:{
      tabBarLabel:'Listings',
      tabBarIcon: ({tintColor})=>(
        <Icon name="map-marker" size={24} />
      )
    }
  },
  Events:{screen:Events,
    navigationOptions:{
      tabBarLabel:'Events',
      tabBarIcon: ({tintColor})=>(
        <Icon name="calendar" size={24} />
      )
    }
  },
  Accounts:{screen:Accounts,
    navigationOptions:{
      tabBarLabel:'Accounts',
      tabBarIcon: ({tintColor})=>(
        <Icon name="user" size={24} />
      )
    }
  },
  More:{screen:More,
    navigationOptions:{
      tabBarLabel:'More',
      tabBarIcon: ({tintColor})=>(
        <Icon name="bars" size={24} />
      )
    }
  }
  },{
    tabBarOptions : {
      style: {
        backgroundColor: 'white',

      }
    }
  })
},
},{
  navigationOptions:{
    header: null
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
},{
  initialRouteName:'Sell'
});
